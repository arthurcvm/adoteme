package br.com.arthurcvm.adoteme;

import com.google.firebase.auth.FirebaseAuthInvalidUserException;

import org.junit.Test;

import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.Models.Vacina;
import br.com.arthurcvm.adoteme.helper.Base64Custom;

import static org.junit.Assert.*;



public class UserFormTest {
    @Test
    public void verificarLogin(){
        Usuario usuario = new Usuario();
        usuario.setEmail("arthurcvm@hotmail.com");
        usuario.setSenha("arthur007pb");
        usuario.setCidade("Cedro");
        usuario.setEstado("Ce");
        usuario.setNascimento("12/12/2012");
        usuario.setNome("Ant");
        //String id = Base64Custom.codificarBse64(usuario.getEmail());
        //usuario.setId(id);Algo ...2233
        assertNotNull(usuario);
    }
    @Test
    public void verificarPet() {
        Pet pet = new Pet();
        pet.setId("wwwwwer3");
        pet.setCor("2we3");
        pet.setImagem("ee");
        pet.setNascimentoPet("10/10/2010");
        pet.setNome("aqqq");
        pet.setPorte("grand");
        pet.setTipo("Cachorro");
        assertNotNull(pet);/////
    }
    @Test
    public void verificarVacina(){
        Vacina vacina = new Vacina();
        vacina.setDataVacina("12/12/2012");
        vacina.setDose("Unica");
        vacina.setIdPet("wrwr32445");
        vacina.setId("sdfghjkllk267");
        vacina.setIdUsuario("wertr2345");
        assertNotNull(vacina);
    }
}
