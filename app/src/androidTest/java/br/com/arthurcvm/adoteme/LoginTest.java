package br.com.arthurcvm.adoteme;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<Login> mActivityRule = new ActivityTestRule<>(Login.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.editTextEmailID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextSenhaID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnEntrarID)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewSeCadastrarID)).check(matches(isDisplayed()));
    }

    @Test
    public void whenPasswordIsEmpty_andClickOnLoginButton_shouldDisplayDialog() {
        testEmptyFieldState(R.id.editTextEmailID);
    }

    @Test
    public void whenUserNameIsEmpty_andClickOnLoginButton_shouldDisplayDialog() {
        testEmptyFieldState(R.id.editTextSenhaID);
    }

    private void testEmptyFieldState(int notEmptyFieldId){
        onView(withId(notEmptyFieldId)).perform(typeText("umTextoDeteste"), closeSoftKeyboard());
        onView(withId(R.id.btnEntrarID)).perform(click());
        onView(withText(R.string.error)).check(matches(isDisplayed()));
        onView(withText(R.string.ok)).perform(click());
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnLoginButton_shouldOpenMainActivity() throws InterruptedException {
        Intents.init();
        onView(withId(R.id.editTextEmailID)).perform(typeText("arthurcvm@hotmail.com"), closeSoftKeyboard());
        onView(withId(R.id.editTextSenhaID)).perform(typeText("arthur007pb"), closeSoftKeyboard());
        Matcher<Intent> matcher = hasComponent(MainActivity.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnEntrarID)).perform(click());
        Thread.sleep(1000);
        intended(matcher);
        Intents.release();
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnCadastrarButton_shouldOpenUserForm() throws InterruptedException {
        Intents.init();
        Matcher<Intent> matcher = hasComponent(UserForm.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.textViewSeCadastrarID)).perform(click());
        Thread.sleep(1000);
        intended(matcher);
        Intents.release();
    }
}
