package br.com.arthurcvm.adoteme;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class PetFormActivityTest {
    @Rule
    public ActivityTestRule<PetFormActivity> mActivityRule = new ActivityTestRule<>(PetFormActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.voltarID)).check(matches(isDisplayed()));
        onView(withId(R.id.circleImageViewCamID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextNomePetID)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBoxMascID)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBoxFemID)).check(matches(isDisplayed()));
        onView(withId(R.id.combotTipoPetID)).check(matches(isDisplayed()));
        onView(withId(R.id.comboPorteID)).check(matches(isDisplayed()));
        onView(withId(R.id.comboCorID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextNascimentoPetID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnCadastrarPetID)).check(matches(isDisplayed()));
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnCadastrarButton_shouldOpenVaccineFormActivity() {
        Intents.init();
        onView(withId(R.id.editTextNomePetID)).perform(typeText("Nome teste"), closeSoftKeyboard());
        onView(withId(R.id.checkBoxMascID)).perform(click());
        onView(withId(R.id.editTextNascimentoPetID)).perform(typeText("02052018"), closeSoftKeyboard());
        Matcher<Intent> matcher = hasComponent(VaccineFormActivity.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnCadastrarPetID)).perform(click());
        intended(matcher);
        Intents.release();
    }
}
