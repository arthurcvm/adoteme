package br.com.arthurcvm.adoteme;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class PerfilUserTest {
    @Rule
    public ActivityTestRule<PerfilUser> mActivityRule = new ActivityTestRule<>(PerfilUser.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.voltarID)).check(matches(isDisplayed()));
        onView(withId(R.id.circleImageViewCamID)).check(matches(isDisplayed()));
        onView(withId(R.id.usernameView)).check(matches(isDisplayed()));
//        onView(withId(R.id.phoneView)).check(matches(isDisplayed()));
        onView(withId(R.id.cityStateView)).check(matches(isDisplayed()));
        onView(withId(R.id.emailView)).check(matches(isDisplayed()));
        onView(withId(R.id.pets_list)).check(matches(isDisplayed()));
        onView(withId(R.id.btnAdocaoID)).check(matches(isDisplayed()));
    }
}
