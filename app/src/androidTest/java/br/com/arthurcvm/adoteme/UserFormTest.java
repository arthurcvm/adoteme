package br.com.arthurcvm.adoteme;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class UserFormTest {

    @Rule
    public ActivityTestRule<UserForm> mActivityRule = new ActivityTestRule<>(UserForm.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.profile_image)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextNomeID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextNascimentoID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextUsuarioID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextEmailID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextSenhaID)).check(matches(isDisplayed()));
        onView(withId(R.id.comboEstadoID)).check(matches(isDisplayed()));
        onView(withId(R.id.comboCidadeID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnCadastrarID)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewLoginID)).check(matches(isDisplayed()));
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnCadastrarButton_shouldOpenMainActivity() throws InterruptedException {
        Intents.init();
        onView(withId(R.id.editTextNomeID)).perform(typeText("Nome teste"), closeSoftKeyboard());
        onView(withId(R.id.editTextNascimentoID)).perform(typeText("06041997"), closeSoftKeyboard());
        onView(withId(R.id.editTextUsuarioID)).perform(typeText("testando"), closeSoftKeyboard());
        onView(withId(R.id.editTextEmailID)).perform(typeText("teste@teste.com"), closeSoftKeyboard());
        onView(withId(R.id.editTextSenhaID)).perform(typeText("123456"), closeSoftKeyboard());
        Matcher<Intent> matcher = hasComponent(MainActivity.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnCadastrarID)).perform(click());
        Thread.sleep(1000);
        intended(matcher);
        Intents.release();
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnLoginButton_shouldOpenUserForm() throws InterruptedException {
        Intents.init();
        Matcher<Intent> matcher = hasComponent(Login.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.textViewLoginID)).perform(click());
        Thread.sleep(2000);
        intended(matcher);
        Intents.release();
    }
}
