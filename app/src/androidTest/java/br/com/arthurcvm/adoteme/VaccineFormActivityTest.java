package br.com.arthurcvm.adoteme;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class VaccineFormActivityTest {

    @Rule
    public ActivityTestRule<VaccineFormActivity> mActivityRule = new ActivityTestRule<>(VaccineFormActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.comboBoxVacinaID)).check(matches(isDisplayed()));
        onView(withId(R.id.comboBoxDoseID)).check(matches(isDisplayed()));
        onView(withId(R.id.editTextDataVacinaID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnSalvarVacinaID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnVoltarID)).check(matches(isDisplayed()));
        onView(withId(R.id.listViewVacinas)).check(matches(isDisplayed()));
    }

    @Test
    public void whenBothFieldsAreFilled_andClickOnVoltarButton_shouldOpenMainActivity() throws InterruptedException {
        Intents.init();
        Matcher<Intent> matcher = hasComponent(MainActivity.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnVoltarID)).perform(click());
        Thread.sleep(1000);
        intended(matcher);
        Intents.release();
    }
}
