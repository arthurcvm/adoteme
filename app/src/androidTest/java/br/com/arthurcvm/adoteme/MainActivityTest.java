package br.com.arthurcvm.adoteme;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState(){
        onView(withId(R.id.btnPerfilID)).check(matches(isDisplayed()));
        onView(withId(R.id.appTitle)).check(matches(isDisplayed()));
        onView(withId(R.id.btnCachorrosID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnGatosID)).check(matches(isDisplayed()));
//        onView(withId(R.id.rl_container_fragmento)).check(matches(isDisplayed()));
        onView(withId(R.id.voltarID)).check(matches(isDisplayed()));
        onView(withId(R.id.avancarID)).check(matches(isDisplayed()));
        onView(withId(R.id.btnAdocaoID)).check(matches(isDisplayed()));
    }

    @Test
    public void whenClickOnUserImage_shouldOpenPerfilUser() {
        Intents.init();
        Matcher<Intent> matcher = hasComponent(PerfilUser.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnPerfilID)).perform(click());
        intended(matcher);
        Intents.release();
    }

    @Test
    public void whenClickOnUserAdocaoButton_shouldOpenPetFormActivity() {
        Intents.init();
        Matcher<Intent> matcher = hasComponent(PetFormActivity.class.getName());

        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);

        onView(withId(R.id.btnAdocaoID)).perform(click());
        intended(matcher);
        Intents.release();
    }

}
