package br.com.arthurcvm.adoteme.Models;

import android.net.Uri;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.storage.StorageReference;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.helper.Base64Custom;

public class Pet {
    private String id;
    private String idUser;
    private String nome;
    private String sexo;
    private String tipo;
    private String porte;
    private String cor;
    private String nascimentoPet;
    private String imagem;


    public void salvar(String identificadorUsuario){
        this.setId(Base64Custom.codificarBse64(this.getNome()+this.getNascimentoPet()));
        DatabaseReference referenceFirebase = ConfiguracaoFirebase.getFirebase();
        referenceFirebase.child("pets").child(identificadorUsuario).child(getId()).setValue(this);
    }
        //pets -> id_responsavel_pet -> id_do_pet = pet em si (valores)

    public Pet() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() { return idUser; }

    public void setIdUser(String idUser) { this.idUser = idUser; }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getNascimentoPet() {
        return nascimentoPet;
    }

    public void setNascimentoPet(String nascimentoPet) {
        this.nascimentoPet = nascimentoPet;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
}
