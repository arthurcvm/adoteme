package br.com.arthurcvm.adoteme;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.helper.Base64Custom;
import br.com.arthurcvm.adoteme.helper.Preferencias;
import de.hdodenhof.circleimageview.CircleImageView;

public class PetFormActivity extends AppCompatActivity {
    private EditText nomePet;
    private RadioButton masculino;
    private RadioButton feminino;
    private Spinner comboTipoPet;
    private Spinner comboPorte;
    private Spinner comboCor;
    private EditText nascimentoPet;
    private Button btnCadastrarPet;
    private CircleImageView btnCamImagem;
    private AppCompatImageView btnVoltar;

    private Pet pet;
    private DatabaseReference firebase;
    private Preferencias usuarioReferencia;
    private FirebaseAuth usuarioFirebase;
    //Uploa de imagem
    private StorageReference mStorage;
    private DatabaseReference mDatabase;
    private StorageTask mUpdateTask;
    //private de.hdodenhof.circleimageview.CircleImageView imageView;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_form);

        nomePet = findViewById(R.id.editTextNomePetID);
        masculino = findViewById(R.id.checkBoxMascID);
        feminino = findViewById(R.id.checkBoxFemID);
        comboTipoPet = findViewById(R.id.combotTipoPetID);
        comboPorte = findViewById(R.id.comboPorteID);
        comboCor = findViewById(R.id.comboCorID);
        nascimentoPet = findViewById(R.id.editTextNascimentoPetID);
        btnCadastrarPet = findViewById(R.id.btnCadastrarPetID);
        btnCamImagem = findViewById(R.id.circleImageViewCamID);
        btnVoltar = findViewById(R.id.voltarID);


        mDatabase = FirebaseDatabase.getInstance().getReference("uploads");


        SimpleMaskFormatter mask = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher mtw = new MaskTextWatcher(nascimentoPet, mask);
        nascimentoPet.addTextChangedListener(mtw);

        ArrayList<String> arrayTipoPet = new ArrayList<String>();
        arrayTipoPet.add("Gato");
        arrayTipoPet.add("Cachorro");

        ArrayList<String> arrayPorte = new ArrayList<String>();
        arrayPorte.add("Pequeno");
        arrayPorte.add("Médio");
        arrayPorte.add("Grande");

        ArrayList<String> arrayCores = new ArrayList<String>();
        arrayCores.add("Branco");
        arrayCores.add("Preto");
        arrayCores.add("Branco&Preto");

        ArrayAdapter<String> adpatadorTipoPet = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayTipoPet
        );

        ArrayAdapter<String> adpatadorPorte = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayPorte
        );

        ArrayAdapter<String> adpatadorCor = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayCores
        );

        comboTipoPet.setAdapter(adpatadorTipoPet);
        comboPorte.setAdapter(adpatadorPorte);
        comboCor.setAdapter(adpatadorCor);

        btnCadastrarPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validaCamposCadastroPet();
                //limparCampos();


            }
        });

        btnCamImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PetFormActivity.this, "CLICOU CAM", Toast.LENGTH_LONG).show();
                openFileChooser();
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void validaCamposCadastroPet(){
        if(mUpdateTask != null){
            Toast.makeText(PetFormActivity.this,"Já Cadastrado",Toast.LENGTH_LONG).show();
        }else{
            if(nomePet.getText().toString().equals("") || nascimentoPet.getText().toString().equals("") || (!feminino.isChecked() && !masculino.isChecked())){
                Toast.makeText(PetFormActivity.this, "Não deixe campos vazios", Toast.LENGTH_LONG).show();
            } else{
                pet = new Pet();
                pet.setNome(nomePet.getText().toString());
                if(feminino.isChecked()){
                    pet.setSexo(feminino.getText().toString());
                } else if(masculino.isChecked()){
                    pet.setSexo(masculino.getText().toString());
                } else{}

                int indiceTipoPet = comboTipoPet.getSelectedItemPosition();
                pet.setTipo(comboTipoPet.getItemAtPosition(indiceTipoPet).toString());

                int indicePorte = comboPorte.getSelectedItemPosition();
                pet.setPorte(comboPorte.getItemAtPosition(indicePorte).toString());

                int indiceCor = comboCor.getSelectedItemPosition();
                pet.setCor(comboCor.getItemAtPosition(indiceCor).toString());
                pet.setNascimentoPet(nascimentoPet.getText().toString());

                cadastraPet();
            }

        }
    }

    private void cadastraPet(){
        // GERA O ID DO PET COM SEU NOME E NASCIMENTO
        //pet.setId(Base64Custom.codificarBse64(pet.getNome()+pet.getNascimentoPet()));
//        Log.i("RESULT", "ID DO PET: " + Base64Custom.codificarBse64(pet.getNome()+pet.getNascimentoPet()));
        usuarioReferencia = new Preferencias(PetFormActivity.this);
        //PASSA O USUARIO RESPONSAVEL
        pet.salvar(usuarioReferencia.getDadosUsuario().get("identificador"));

        String imagem_pet = upLoadImagem();
        Log.i("imagem capturada", "imagem capturada: " + imagem_pet);
        pet.setImagem(imagem_pet);

        Intent intent = new Intent(PetFormActivity.this, VaccineFormActivity.class);
        intent.putExtra("petId", pet.getId());
        intent.putExtra("petNome", pet.getNome());
        intent.putExtra("petNascimento", pet.getNascimentoPet());
        finish();
        startActivity( intent );
    }

    private void limparCampos(){
        nomePet.setText("");
        masculino.setChecked(false);
        feminino.setChecked(false);
        nascimentoPet.setText("");
    }
    //upload Imagem
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            //pet.setImaUrl(mImageUri);
            Picasso.with(this).load(mImageUri).into(btnCamImagem);
        }
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    private String upLoadImagem() {
        String imagem = null;
        if (mImageUri != null) {
            mStorage = FirebaseStorage.getInstance().getReference(usuarioReferencia.getDadosUsuario().get("identificador"));
            StorageReference fileReference = mStorage.child(pet.getId()
                    + "." + getFileExtension(mImageUri));

            imagem = String.valueOf(mStorage.child(pet.getId()+ "." +getFileExtension(mImageUri)));

            Log.i("PET_IMAGE", "PET_IMAGE: " + imagem);

            mUpdateTask = fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                  Toast.makeText(PetFormActivity.this,"Updade sucesso",Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }

        return imagem;
    }
}
