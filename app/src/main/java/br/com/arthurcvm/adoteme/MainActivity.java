package br.com.arthurcvm.adoteme;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.yarolegovich.discretescrollview.DSVOrientation;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.adapter.Item;
import br.com.arthurcvm.adoteme.adapter.ListViewPerfilUser;
import br.com.arthurcvm.adoteme.adapter.ShopAdapter;
import br.com.arthurcvm.adoteme.helper.Preferencias;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements DiscreteScrollView.OnItemChangedListener,
        View.OnClickListener {
    private Toolbar toolbar;
    private CircleImageView btnPerfil;
    private DiscreteScrollView itemPicker;
    private InfiniteScrollAdapter infiniteAdapter;
    private DatabaseReference conexao;
    private Preferencias dadosUsuario;
    private Button btnAdocao;
    private Button btnCachorrosID;
    private Button btnGatosID;
    private TextView petName;
    private FirebaseAuth usuarioAutenticacao;
    private Usuario usuario;
    private static List<Pet> arrayPets;
    private List<Pet> arrayCachorros;
    private List<Pet> arrayGatos;

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMyToken();

        usuarioAutenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();

        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        btnPerfil = findViewById(R.id.btnPerfilID);
        btnAdocao = findViewById(R.id.btnAdocaoID);
        btnCachorrosID = findViewById(R.id.btnCachorrosID);
        btnGatosID = findViewById(R.id.btnGatosID);
        petName = (TextView) findViewById(R.id.petName);

        //Configurações da toolbar
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        btnPerfil.setOnClickListener(this);
        btnAdocao.setOnClickListener(this);
        btnCachorrosID.setOnClickListener(this);
        btnGatosID.setOnClickListener(this);

        dadosUsuario = new Preferencias(MainActivity.this);
        //Log.i("RESULTADO_PREFERENCIAS", "RESULTADO_PREFERENCIAS "+ preferencias.getDadosUsuario().get("identificador"));

        conexao = ConfiguracaoFirebase.getFirebase().child("usuarios").child(dadosUsuario.getDadosUsuario().get("identificador"));
        conexao.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario = dataSnapshot.getValue(Usuario.class);

                ConfiguracaoFirebase.getFirebaseStorage()
                        .child(dadosUsuario.getDadosUsuario().get("identificador"))
                        .child(usuario.getId()+".jpg").getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                usuario.setImagem(uri.toString());
                                Picasso.with(getApplicationContext()).load(usuario.getImagem()).into(btnPerfil);
//                                Toast.makeText(MainActivity.this, "Setou imagem", Toast.LENGTH_SHORT).show();

                                if(!usuario.getNotificationToken().equals(token)){
                                    usuario.atualizaToken(token);
                                }

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(MainActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                    }
                });

                conexao = ConfiguracaoFirebase.getFirebase().child("pets");
                conexao.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        arrayPets = new ArrayList<Pet>();
                        arrayCachorros = new ArrayList<Pet>();
                        arrayGatos = new ArrayList<Pet>();
//                ArrayList<StorageReference> imagens = new ArrayList<StorageReference>();
//                ArrayList<String> sexo = new ArrayList<String>();

                        for (DataSnapshot dados : dataSnapshot.getChildren()) {
                            if (!dados.getKey().equals(usuario.getId())) {
                                for (DataSnapshot dataPet : dados.getChildren()) {
                                    final Pet pet = dataPet.getValue(Pet.class);
                                    pet.setIdUser(dados.getKey());
                                    arrayPets.add(pet);
                                }
                            }
                        }

                        for (final Pet pet : arrayPets){
                            ConfiguracaoFirebase.getFirebaseStorage()
                                    .child(pet.getIdUser())
                                    .child(pet.getId()+".jpg").getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            pet.setImagem(uri.toString());
                                            if (pet.getTipo().equals("Cachorro")){
                                                arrayCachorros.add(pet);
                                            }
                                            else if (pet.getTipo().equals("Gato")){
                                                arrayGatos.add(pet);
                                            }

                                            infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(arrayPets));
                                            itemPicker.setAdapter(infiniteAdapter);
                                            itemPicker.setItemTransitionTimeMillis(200);
                                            itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                                                    .setMinScale(0.8f)
                                                    .build());
//                                            onItemChanged(arrayPets.get(0));
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(MainActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        conexao = ConfiguracaoFirebase.getFirebase().child("pets");
//        conexao.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                arrayPets = new ArrayList<Pet>();
////                ArrayList<StorageReference> imagens = new ArrayList<StorageReference>();
////                ArrayList<String> sexo = new ArrayList<String>();
//
//                for (DataSnapshot dados : dataSnapshot.getChildren()) {
//                    if (dados.getKey() != usuario.getId()) {
//                        for (DataSnapshot dataPet : dados.getChildren()) {
//                            final Pet pet = dataPet.getValue(Pet.class);
//                            pet.setIdUser(dados.getKey());
//                            arrayPets.add(pet);
//                        }
//                    }
//                }
//                final int tamanho = arrayPets.size();
//                for (final Pet pet : arrayPets){
//                    ConfiguracaoFirebase.getFirebaseStorage()
//                            .child(dadosUsuario.getDadosUsuario().get("identificador"))
//                            .child(pet.getId()+".jpg").getDownloadUrl()
//                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                @Override
//                                public void onSuccess(Uri uri) {
//                                    pet.setImagem(uri.toString());
////                                    arrayPets.add(pet);
////                            Toast.makeText(PerfilUser.this, "Setou imagem", Toast.LENGTH_SHORT).show();
//                                    if (arrayPets.size() == tamanho){
//                                        infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(arrayPets));
//                                        itemPicker.setAdapter(infiniteAdapter);
//                                        itemPicker.setItemTransitionTimeMillis(200);
//                                        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
//                                                .setMinScale(0.8f)
//                                                .build());
//                                        onItemChanged(arrayPets.get(0));
//                                    }
//                                }
//                            }).addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception exception) {
//                            Toast.makeText(MainActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

//        conexao = ConfiguracaoFirebase.getFirebase().child("pets").child(dadosUsuario.getDadosUsuario().get("identificador"));
//        conexao.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                arrayPets = new ArrayList<Pet>();
////                ArrayList<StorageReference> imagens = new ArrayList<StorageReference>();
////                ArrayList<String> sexo = new ArrayList<String>();
//
//                for(DataSnapshot dados : dataSnapshot.getChildren()){
//                    final int tamanho = (int) dados.getChildrenCount()/7;
//                    final Pet pet = dados.getValue(Pet.class);
//
//                    ConfiguracaoFirebase.getFirebaseStorage()
//                            .child(dadosUsuario.getDadosUsuario().get("identificador"))
//                            .child(pet.getId()+".jpg").getDownloadUrl()
//                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                @Override
//                                public void onSuccess(Uri uri) {
//                                    pet.setImagem(uri.toString());
//                                    arrayPets.add(pet);
////                            Toast.makeText(PerfilUser.this, "Setou imagem", Toast.LENGTH_SHORT).show();
//                                    if (arrayPets.size() == tamanho){
//                                        infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(arrayPets));
//                                        itemPicker.setAdapter(infiniteAdapter);
//                                        itemPicker.setItemTransitionTimeMillis(200);
//                                        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
//                                                .setMinScale(0.8f)
//                                                .build());
//                                        onItemChanged(arrayPets.get(0));
//                                    }
//                                }
//                            }).addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception exception) {
//                            Toast.makeText(MainActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            }
//
//
//
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });



        itemPicker = (DiscreteScrollView) findViewById(R.id.item_picker);
        itemPicker.setOrientation(DSVOrientation.HORIZONTAL);
        itemPicker.addOnItemChangedListener(this);

    }

    //Create para "inflar" (exibir) menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btnSair){
            deslogarUsuario();
            return true;
        }
        return true;
    }

    public void deslogarUsuario(){
        usuarioAutenticacao.signOut();
        Intent intent = new Intent(MainActivity.this, Login.class);
        startActivity(intent);
        finish();
    }

    private void onItemChanged(Pet pet) {
        petName.setText(pet.getNome());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdocaoID:
                int realPosition = infiniteAdapter.getRealPosition(itemPicker.getCurrentItem());
                Pet current = arrayPets.get(realPosition);
                Intent PetProfile = new Intent(MainActivity.this, PetProfileActivity.class);
                PetProfile.putExtra("petId", current.getId());
                PetProfile.putExtra("userId", current.getIdUser());
                PetProfile.putExtra("interestedId", usuario.getId());
                finish();
                startActivity( PetProfile );
                break;
            case R.id.btnPerfilID:
                Intent PerfilUser = new Intent(MainActivity.this, PerfilUser.class);
                startActivity( PerfilUser );
                break;

            case R.id.btnCachorrosID:
                arrayPets = new ArrayList<Pet>();
                arrayPets.addAll(arrayCachorros);
//                for (Pet pet : arrayGatos){
//                    arrayPets.remove(pet);
//                }

                infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(arrayPets));
                itemPicker.setAdapter(infiniteAdapter);
                itemPicker.setItemTransitionTimeMillis(200);
                itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                        .setMinScale(0.8f)
                        .build());

                break;
            case R.id.btnGatosID:
                arrayPets = new ArrayList<Pet>();
                arrayPets.addAll(arrayGatos);
//                for (Pet pet : arrayGatos){
//                    arrayPets.add(pet);
//                }

                infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(arrayPets));
                itemPicker.setAdapter(infiniteAdapter);
                itemPicker.setItemTransitionTimeMillis(200);
                itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                        .setMinScale(0.8f)
                        .build());
//                onItemChanged(arrayPets.get(0));

                break;
            default:
                //TODO
                break;
        }
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int position) {
        int positionInDataSet = infiniteAdapter.getRealPosition(position);
        onItemChanged(arrayPets.get(positionInDataSet));
    }

    public void getMyToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Token", "getInstanceId failed", task.getException());
                            token = null;
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();

                        // Log
                        Log.d("Token", token);
                    }
                });
    }
}
