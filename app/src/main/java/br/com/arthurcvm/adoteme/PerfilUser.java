package br.com.arthurcvm.adoteme;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.adapter.ListViewPerfilUser;
import br.com.arthurcvm.adoteme.helper.Preferencias;
import de.hdodenhof.circleimageview.CircleImageView;

public class PerfilUser extends AppCompatActivity {
    private ListView listViewPets;
    private DatabaseReference conexao;
    private Preferencias preferencias;
    private CircleImageView circleImageViewCamID;
    private TextView nome;
    private TextView cidadeEstado;
    private TextView email;
    private Usuario usuario;

    private AppCompatImageView btnVoltar;
    private Button btnAdocao;

    private StorageReference referenciaStorage;
    ArrayList<Pet> arrayPets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_user);
        listViewPets = (ListView) findViewById(R.id.pets_list);

        circleImageViewCamID = findViewById(R.id.circleImageViewCamID);
        nome = findViewById(R.id.usernameView);
        cidadeEstado = findViewById(R.id.cityStateView);
        email = findViewById(R.id.emailView);

        btnVoltar = findViewById(R.id.voltarID);
        btnAdocao = findViewById(R.id.btnAdocaoID);

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerfilUser.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnAdocao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerfilUser.this, PetFormActivity.class);
                startActivity(intent);
                finish();
            }
        });

        preferencias = new Preferencias(PerfilUser.this);
        //Log.i("RESULTADO_PREFERENCIAS", "RESULTADO_PREFERENCIAS "+ preferencias.getDadosUsuario().get("identificador"));

        conexao = ConfiguracaoFirebase.getFirebase().child("usuarios").child(preferencias.getDadosUsuario().get("identificador"));
        conexao.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario = dataSnapshot.getValue(Usuario.class);

                ConfiguracaoFirebase.getFirebaseStorage()
                        .child(preferencias.getDadosUsuario().get("identificador"))
                        .child(usuario.getId()+".jpg").getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                usuario.setImagem(uri.toString());
                                Picasso.with(getApplicationContext()).load(usuario.getImagem()).into(circleImageViewCamID);
//                                Toast.makeText(PerfilUser.this, "Setou imagem", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(PerfilUser.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                    }
                });

                nome.setText(usuario.getNome());
                cidadeEstado.setText(usuario.getCidade() + "/" + usuario.getEstado());
                email.setText(usuario.getEmail());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        conexao = ConfiguracaoFirebase.getFirebase().child("pets").child(preferencias.getDadosUsuario().get("identificador"));
        conexao.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrayPets = new ArrayList<Pet>();
//                ArrayList<StorageReference> imagens = new ArrayList<StorageReference>();
//                ArrayList<String> sexo = new ArrayList<String>();

                for(DataSnapshot dados : dataSnapshot.getChildren()){
                    final int tamanho = (int) dados.getChildrenCount()/7;
                    final Pet pet = dados.getValue(Pet.class);

                    ConfiguracaoFirebase.getFirebaseStorage()
                            .child(preferencias.getDadosUsuario().get("identificador"))
                            .child(pet.getId()+".jpg").getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            pet.setImagem(uri.toString());
                            arrayPets.add(pet);
//                            Toast.makeText(PerfilUser.this, "Setou imagem", Toast.LENGTH_SHORT).show();
                            if (arrayPets.size() == tamanho){
                                ListViewPerfilUser adapter = new ListViewPerfilUser(
                                        getApplicationContext(),
                                        R.layout.list_view_personalizado_perfil_user,
                                        arrayPets
                                );

                                listViewPets.setAdapter(adapter);
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(PerfilUser.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                        }
                    });


//                    Task<Uri> imagemUri = ConfiguracaoFirebase.getFirebaseStorage()
//                            .child(preferencias.getDadosUsuario().get("identificador"))
//                            .child(pet.getId()+".jpg").getDownloadUrl();
//                    pet.setImagem(imagemUri.getResult().toString());
//                    arrayPets.add(pet);
//
//                    arrayPets.add(pet.getNome());
//                    sexo.add(pet.getSexo());
//                    imagens.add(referenciaStorage);
                }


//                ArrayList<HashMap<String, String>> dados = new ArrayList<HashMap<String, String>>();
//                for(int i=0; i < arrayPets.size(); i++){
//                    HashMap<String, String> pet_valores = new HashMap<String, String>();
//                    pet_valores.put("listview_nome_pet", arrayPets.get(i));
//                    pet_valores.put("listview_sexo_pet", sexo.get(i));
//                    pet_valores.put("perfil_user_id", String.valueOf(imagens.get(i)));
//                    dados.add(pet_valores);
//                }
//
//                String[] from = {"perfil_user_id", "listview_nome_pet", "listview_sexo_pet"};
//                int[] to = {R.id.perfil_user_id, R.id.listview_nome_pet_id, R.id.listview_sexo_pet_id};
//                SimpleAdapter simpleAdapter = new SimpleAdapter(
//                        getBaseContext(),
//                        dados,
//                        R.layout.list_view_personalizado_perfil_user,
//                        from,
//                        to);
//
//

//                ListViewPerfilUser adapter = new ListViewPerfilUser(
//                        getApplicationContext(),
//                        R.layout.list_view_personalizado_perfil_user,
//                        arrayPets
//                );
//
//                listViewPets.setAdapter(adapter);
            }




            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //referenciaStorage = ConfiguracaoFirebase.getFirebaseStorage().child(usuario.getId());
        //Log.i("PETS", "PETS: " + conexao);

//
//        ArrayList<String> arrayPets = new ArrayList<String>();
//        arrayPets.add("gato 1");
//        arrayPets.add("gato 2");
//        arrayPets.add("cao 1");
//        arrayPets.add("cao 2");
//
//        ArrayList<String> sexo = new ArrayList<String>();
//        sexo.add("F");
//        sexo.add("M");
//        sexo.add("F");
//        sexo.add("M");
//
//        int[] imagens = new int[]{
//                R.drawable.a, R.drawable.a, R.drawable.a, R.drawable.a,
//        };

//        ArrayList<HashMap<String, String>> dados = new ArrayList<HashMap<String, String>>();
//        for(int i=0; i < arrayPets.size(); i++){
//            HashMap<String, String> pet_valores = new HashMap<String, String>();
//            pet_valores.put("listview_nome_pet", arrayPets.get(i));
//            pet_valores.put("listview_sexo_pet", sexo.get(i));
//            pet_valores.put("perfil_user_id", Integer.toString(imagens[i]));
//            dados.add(pet_valores);
//        }
//
//        String[] from = {"perfil_user_id", "listview_nome_pet", "listview_sexo_pet"};
//        int[] to = {R.id.perfil_user_id, R.id.listview_nome_pet_id, R.id.listview_sexo_pet_id};
//        SimpleAdapter simpleAdapter = new SimpleAdapter(
//                getBaseContext(),
//                dados,
//                R.layout.list_view_personalizado_perfil_user,
//                from,
//                to);

//        ArrayAdapter<String> adapterPets = new ArrayAdapter<String>(
//                getApplicationContext(),
//                android.R.layout.simple_list_item_1,
//                android.R.id.text1,
//                arrayPets
//        );
//        listViewPets.setAdapter(simpleAdapter);


    }
}
