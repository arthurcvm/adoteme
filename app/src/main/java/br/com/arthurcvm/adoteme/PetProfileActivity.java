package br.com.arthurcvm.adoteme;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.Models.Vacina;
import br.com.arthurcvm.adoteme.adapter.ShopAdapter;
import br.com.arthurcvm.adoteme.helper.Preferencias;
import de.hdodenhof.circleimageview.CircleImageView;

public class PetProfileActivity extends AppCompatActivity {
    private Button btnAdopt;
    private TextView pet_name;
    private TextView age;
    private TextView sex;
    private TextView color;
    private TextView size;
    private ImageView imgPet;
    private ListView listaVacinas;
    private CircleImageView btnPerfilID;
    private TextView owner_name;
    private String petId;
    private String userId;
    private String interestedId;
    private DatabaseReference conexao;
//    private Preferencias preferencias;
    private Pet pet;
    private Usuario userOwner;
    private ArrayList<Vacina> vacinasList;
    private ArrayList<String> vacinasCadastradas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_profile);

        btnAdopt = findViewById(R.id.btnAdopt);
        pet_name = findViewById(R.id.pet_name);
        age = findViewById(R.id.age);
        sex = findViewById(R.id.sex);
        color = findViewById(R.id.color);
        size = findViewById(R.id.size);
        imgPet = findViewById(R.id.imgPet);
        listaVacinas = findViewById(R.id.vaccines_list);
        btnPerfilID = findViewById(R.id.btnPerfilID);
        owner_name = findViewById(R.id.owner_name);

        btnAdopt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adotar();
            }
        });

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            petId = extra.getString("petId");
            userId = extra.getString("userId");
            interestedId = extra.getString("interestedId");
        }

//        preferencias = new Preferencias(PetProfileActivity.this);
        conexao = ConfiguracaoFirebase.getFirebase().child("pets");
        conexao.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dados : dataSnapshot.getChildren()){
                    if (dados.getKey().equals(userId)) {
                        for (DataSnapshot dataPet : dados.getChildren()) {
                            if (dataPet.getValue(Pet.class).getId().equals(petId)) {
                                pet = dataPet.getValue(Pet.class);
                                pet.setIdUser(userId);
                                pet_name.setText(pet.getNome());
                                age.setText(pet.getNascimentoPet());
                                sex.setText(pet.getSexo());
                                color.setText(pet.getCor());
                                size.setText(pet.getPorte());
                            }
                        }
                    }
                }

                ConfiguracaoFirebase.getFirebaseStorage()
                        .child(pet.getIdUser())
                        .child(pet.getId()+".jpg").getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                pet.setImagem(uri.toString());
                                Picasso.with(getApplicationContext()).load(pet.getImagem()).into(imgPet);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(PetProfileActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                    }
                });

                conexao = ConfiguracaoFirebase.getFirebase().child("vacinas").child(userId).child(petId);
                conexao.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        vacinasList = new ArrayList<>();
                        for (DataSnapshot dados : dataSnapshot.getChildren()) {
                            Vacina vacina = dados.getValue(Vacina.class);
                            vacinasList.add(vacina);
                            vacinasCadastradas.add(vacina.getDataVacina() + " - " +vacina.getVacina() + " - " +vacina.getDose());
                            listaVacinasCadastradas();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                conexao = ConfiguracaoFirebase.getFirebase().child("usuarios").child(userId);
                conexao.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userOwner = dataSnapshot.getValue(Usuario.class);
                        owner_name.setText(userOwner.getNome());

                        ConfiguracaoFirebase.getFirebaseStorage()
                                .child(userId)
                                .child(userId+".jpg").getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        userOwner.setImagem(uri.toString());
                                        Picasso.with(getApplicationContext()).load(userOwner.getImagem()).into(btnPerfilID);
//                                Toast.makeText(MainActivity.this, "Setou imagem", Toast.LENGTH_SHORT).show();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Toast.makeText(PetProfileActivity.this, "Erro na imagem", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void listaVacinasCadastradas(){
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                PetProfileActivity.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                vacinasCadastradas
        );

        listaVacinas.setAdapter(adaptador);
    }

    private void adotar(){
        conexao = ConfiguracaoFirebase.getFirebase().child("pets").child(userId).child(petId);
        conexao.child("interestedUid").child(interestedId).setValue(true);
    }
}
