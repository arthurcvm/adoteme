package br.com.arthurcvm.adoteme;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.helper.Base64Custom;
import br.com.arthurcvm.adoteme.helper.Preferencias;

public class Login extends AppCompatActivity {

    private EditText email;
    private EditText senha;
    private Button btnEntrar;
    private TextView seCadastrar;
    private Usuario usuario;
    private FirebaseAuth autenticacao;
    private Preferencias preferencias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        verificaUsuarioLogado();

        email = findViewById(R.id.editTextEmailID);
        senha = findViewById(R.id.editTextSenhaID);
        btnEntrar = findViewById(R.id.btnEntrarID);
        seCadastrar = findViewById(R.id.textViewSeCadastrarID);

        seCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirCadastroUsuario();
            }
        });

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarCamposLogin();
            }
        });
    }

   public void validarLogin(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.signInWithEmailAndPassword(usuario.getEmail(), usuario.getSenha())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    salvaPreferenciasUsuario(task.getResult().getUser().getUid(), usuario.getNome());
                    abrirTelaPrincipal();
                    finish();
                } else{
                    String excessao = "";
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthInvalidUserException e){excessao = "Usuário invalido, tente novamente";}
                        catch (FirebaseAuthInvalidCredentialsException e){excessao = "Senha invalida, tente novamente";}
                        catch (Exception e) {excessao = "Tente se logar novamente";e.printStackTrace(); }
                    Toast.makeText(Login.this, "Erro. "+excessao, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void validarCamposLogin(){
        if(email.getText().toString().equals("") || senha.getText().toString().equals("")){
            Toast.makeText(Login.this, "É necessário preencher os campos para logar", Toast.LENGTH_LONG).show();
        } else{
            usuario = new Usuario();
            usuario.setEmail(email.getText().toString());
            usuario.setSenha(senha.getText().toString());
            validarLogin();
        }
    }

    private void salvaPreferenciasUsuario(String uid, String nome){
//        String identificadorUsuario = Base64Custom.codificarBse64(email);
        preferencias = new Preferencias(Login.this);
        preferencias.salvarUsuarioPreferencias(uid, nome);
        Log.i("RESULTADO_PREFERENCIAS", "RESULTADO_PREFERENCIAS "+ preferencias.getDadosUsuario().get("identificador"));
        //preferencias.deleteDadosUsuario();
        //Log.i("USUARIO FIREBASE", "USUARIO FIREBASE " + userPreferencia.getNome());
        //HashMap<String, String> usuario = preferencias.getDadosUsuario();
        //Log.i("USUARIO", "DADOS "+ usuario.get("usuario"));
    }

    public void abrirCadastroUsuario(){
        Intent intent = new Intent(Login.this, UserForm.class);
        startActivity(intent);
    }
    private void abrirTelaPrincipal(){
        Intent intent =  new Intent(Login.this, MainActivity.class);
        //intent.putExtra("usuario", user);
        startActivity(intent);
        finish();
    }

    public void verificaUsuarioLogado(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        //verifica se o user ta logado
        if(autenticacao.getCurrentUser() != null){
            abrirTelaPrincipal();
        }
    }
}
