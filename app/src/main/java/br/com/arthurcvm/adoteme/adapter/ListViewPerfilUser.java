package br.com.arthurcvm.adoteme.adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.R;

public class ListViewPerfilUser extends ArrayAdapter<Pet> {

    ArrayList<Pet> pets;
    Context contexto;
    int resource;

    public ListViewPerfilUser(Context context, int resource, ArrayList<Pet> pets) {
        super(context, resource, pets);
        this.pets = pets;
        this.contexto = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_view_personalizado_perfil_user, null, true);
        }
        Pet pet = getItem(position);
        ImageView imageView = convertView.findViewById(R.id.listview_imagem_pet_id);
        Picasso.with(contexto).load(pet.getImagem()).into(imageView);

        TextView txtNomePet = (TextView) convertView.findViewById(R.id.listview_nome_pet_id);
        txtNomePet.setText(pet.getNome());

        TextView txtSexoPet = (TextView) convertView.findViewById(R.id.listview_sexo_pet_id);
        txtSexoPet.setText(pet.getSexo());

        return convertView;
    }
}
