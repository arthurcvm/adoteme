package br.com.arthurcvm.adoteme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.security.acl.AclNotFoundException;
import java.util.ArrayList;

import br.com.arthurcvm.adoteme.Models.Pet;
import br.com.arthurcvm.adoteme.Models.Vacina;
import br.com.arthurcvm.adoteme.helper.Base64Custom;
import br.com.arthurcvm.adoteme.helper.Preferencias;

public class VaccineFormActivity extends AppCompatActivity {
    private Spinner comboVacina;
    private Spinner comboDose;
    private EditText dataVacina;

    private Button btnSalvar;
    private Button btnVoltar;

    private Pet pet;
    private Vacina vacinaModelo;
    private Preferencias preferencias;
    private ListView listaVacinas;

    private ArrayList<String> vacinasCadastradas = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine_form);

        comboVacina = findViewById(R.id.comboBoxVacinaID);
        comboDose = findViewById(R.id.comboBoxDoseID);
        dataVacina = findViewById(R.id.editTextDataVacinaID);
        btnSalvar = findViewById(R.id.btnSalvarVacinaID);
        btnVoltar = findViewById(R.id.btnVoltarID);
        listaVacinas = findViewById(R.id.listViewVacinas);

        ArrayList<String> arrayVacina = new ArrayList<String>();
        arrayVacina.add("Vacina A");
        arrayVacina.add("Vacina B");
        arrayVacina.add("Vacina C");

        ArrayList<String> arrayDose = new ArrayList<String>();
        arrayDose.add("DOSE 1");
        arrayDose.add("DOSE 2");
        arrayDose.add("DOSE 3");

        ArrayAdapter<String> adpatadorVacina = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayVacina
        );

        ArrayAdapter<String> adpatadorCor = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayDose
        );

        comboVacina.setAdapter(adpatadorVacina);
        comboDose.setAdapter(adpatadorCor);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataVacina.getText().toString().equals("")){
                    Toast.makeText(VaccineFormActivity.this, "Digite uma data de vacinação", Toast.LENGTH_LONG).show();
                } else{
                    vacinaModelo = new Vacina();

                    int indiceVacina = comboVacina.getSelectedItemPosition();
                    vacinaModelo.setVacina(comboVacina.getItemAtPosition(indiceVacina).toString());
                    int indiceDose = comboDose.getSelectedItemPosition();
                    vacinaModelo.setDose(comboDose.getItemAtPosition(indiceDose).toString());
                    vacinaModelo.setDataVacina(dataVacina.getText().toString());

                    Bundle extra = getIntent().getExtras();
                    if(extra != null){
                        String petId = extra.getString("petId");
                        vacinaModelo.setIdPet(petId);

                        String petNome = extra.getString("petNome");
                        String petNascimento = extra.getString("petNascimento");

                        //GERA O ID DA VACINA COM SABE NO ID DO PET, NASCIMENTO E DATA DA VACINA
                        vacinaModelo.setId(Base64Custom.codificarBse64(petId+petNascimento+vacinaModelo.getDataVacina()));
                        preferencias = new Preferencias(VaccineFormActivity .this);
                        vacinaModelo.setIdUsuario(preferencias.getDadosUsuario().get("identificador"));
                        vacinaModelo.salvar();
                        vacinasCadastradas.add(petNome + " - " +vacinaModelo.getVacina());
                        limpar_campo();
                        listaVacinasCadastradas();
                    }
                }
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void limpar_campo(){
        dataVacina.setText("");
    }

    private void listaVacinasCadastradas(){
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                VaccineFormActivity.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                vacinasCadastradas
        );

        listaVacinas.setAdapter(adaptador);
    }
}
