package br.com.arthurcvm.adoteme.Models;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.helper.Base64Custom;

public class Vacina {
    private String id;
    private String vacina;
    private String dose;
    private String dataVacina;
    private String idPet;
    private String idUsuario;

    public void salvar(){

        DatabaseReference referenceFirebase = ConfiguracaoFirebase.getFirebase();
        referenceFirebase.child("vacinas").child(getIdUsuario()).child(getIdPet()).child(getId()).setValue(this);
        // vacinas -> usuario -> pet -> idVacina => dados vacina
    }

    public Vacina(){

    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVacina() {
        return vacina;
    }

    public void setVacina(String vacina) {
        this.vacina = vacina;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDataVacina() {
        return dataVacina;
    }

    public void setDataVacina(String dataVacina) {
        this.dataVacina = dataVacina;
    }

    public String getIdPet() {
        return idPet;
    }

    @Exclude
    public void setIdPet(String idPet) {
        this.idPet = idPet;
    }

    @Exclude
    public String getIdUsuario() {
        return idUsuario;
    }


    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
