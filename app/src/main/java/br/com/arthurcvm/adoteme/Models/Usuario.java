package br.com.arthurcvm.adoteme.Models;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

import java.util.Date;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Services.PushMessageService;
import br.com.arthurcvm.adoteme.helper.Base64Custom;

public class Usuario {
    private String id;
    private String imagem;
    private String nome;
    private String usuario;
    private String email;
    private String senha;
    private String estado;
    private String cidade;
    private String nascimento;
    private String notificationToken;

    public Usuario() {

    }

    public void salvar(){
//        this.setId(Base64Custom.codificarBse64(this.getEmail()));
        DatabaseReference referenceFirebase = ConfiguracaoFirebase.getFirebase();
        referenceFirebase.child("usuarios").child(getId()).setValue(this);
    }

    public void atualizaToken(String notificationToken){
        this.setNotificationToken(notificationToken);
        DatabaseReference referenceFirebase = ConfiguracaoFirebase.getFirebase();
        referenceFirebase.child("usuarios").child(getId()).child(notificationToken).setValue(notificationToken);
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImagem() { return imagem; }

    public void setImagem(String imagem) { this.imagem = imagem; }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getNotificationToken() { return notificationToken; }

    public void setNotificationToken(String notificationToken) { this.notificationToken = notificationToken; }
}
