package br.com.arthurcvm.adoteme;

import android.content.ContentResolver;
import android.content.Intent;
import android.hardware.usb.UsbRequest;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import br.com.arthurcvm.adoteme.Config.ConfiguracaoFirebase;
import br.com.arthurcvm.adoteme.Models.Usuario;
import br.com.arthurcvm.adoteme.Services.PushMessageService;
import br.com.arthurcvm.adoteme.helper.Base64Custom;
import br.com.arthurcvm.adoteme.helper.Preferencias;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserForm extends AppCompatActivity {
    private EditText nome;
    private EditText nascimento;
    private EditText usuario;
    private EditText email;
    private EditText senha;
    private Spinner estado;
    private Spinner cidade;
    private Button btnCadastrar;
    private CircleImageView btnCamImagem;
    private TextView logar;

    private Usuario usuarioModelo;
    private FirebaseAuth autenticacao;
    private Preferencias preferencia;

    //Upload de imagem
    private StorageReference mStorage;
    private DatabaseReference mDatabase;
    private StorageTask mUpdateTask;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri;

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);

        this.getMyToken();

        nome = findViewById(R.id.editTextNomeID);
        nascimento = findViewById(R.id.editTextNascimentoID);
        usuario = findViewById(R.id.editTextUsuarioID);
        email = findViewById(R.id.editTextEmailID);
        senha = findViewById(R.id.editTextSenhaID);

        estado = findViewById(R.id.comboEstadoID);
        cidade = findViewById(R.id.comboCidadeID);
        btnCadastrar = findViewById(R.id.btnCadastrarID);
        btnCamImagem = findViewById(R.id.circleImageViewCamID);
        logar = findViewById(R.id.textViewLoginID);

        SimpleMaskFormatter mask = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher mtw = new MaskTextWatcher(nascimento, mask);
        nascimento.addTextChangedListener(mtw);

        logar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayList<String> arrayEstados = new ArrayList<String>();
        arrayEstados.add("Acre (AC)");
        arrayEstados.add("Alagoas (AL)");
        arrayEstados.add("Amapá (AP)");
        arrayEstados.add("Amazonas (AM)");
        arrayEstados.add("Bahia (BA)");

        arrayEstados.add("Ceará (CE)");
        /*
        arrayEstados.add("Distrito Federal (DF)");
        arrayEstados.add("Espírito Santo (ES)");
        arrayEstados.add("Goiás (GO)");
        arrayEstados.add("Maranhão (MA)");
        arrayEstados.add("Mato Grosso (MT)");
        arrayEstados.add("Mato Grosso do Sul (MS)");
        arrayEstados.add("Minas Gerais (MG)");
        arrayEstados.add("Pará (PA) ");
        arrayEstados.add("Paraíba (PB)");
        arrayEstados.add("Paraná (PR)");
        arrayEstados.add("Pernambuco (PE)");
        arrayEstados.add("Piauí (PI)");
        arrayEstados.add("Rio de Janeiro (RJ)");
        arrayEstados.add("Rio Grande do Norte (RN)");
        arrayEstados.add("Rio Grande do Sul (RS)");
        arrayEstados.add("Rondônia (RO)");
        arrayEstados.add("Roraima (RR)");
        arrayEstados.add("Santa Catarina (SC)");
        arrayEstados.add("São Paulo (SP)");
        arrayEstados.add("Sergipe (SE)");
        arrayEstados.add("Tocantins (TO)");
        */

        ArrayList<String> arrayCidades = new ArrayList<String>();
        arrayCidades.add("Fortaleza");
        arrayCidades.add("Crato");
        arrayCidades.add("Iguatu");

        ArrayAdapter<String>adpatadorEstados = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayEstados
        );

        ArrayAdapter<String>adpatadorCidades = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                arrayCidades
        );

        estado.setAdapter(adpatadorEstados);
        cidade.setAdapter(adpatadorCidades);

        btnCamImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UserForm.this, "CLICOU CAM", Toast.LENGTH_LONG).show();
                openFileChooser();
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarCampos();
            }
        });
    }
    
    //upload Imagem
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            //pet.setImaUrl(mImageUri);
            Picasso.with(this).load(mImageUri).into(btnCamImagem);
        }
    }

    private void validarCampos(){
        if(nome.getText().toString().equals("") || nascimento.getText().toString().equals("") ||
        usuario.getText().toString().equals("") || email.getText().toString().equals("") || senha.getText().toString().equals("")){
            Toast.makeText(UserForm.this, "Não deixe campos vazios", Toast.LENGTH_LONG).show();
        } else{
            usuarioModelo = new Usuario();
            usuarioModelo.setNome(nome.getText().toString());
            usuarioModelo.setNascimento(nascimento.getText().toString());
            usuarioModelo.setUsuario(usuario.getText().toString());
            usuarioModelo.setEmail(email.getText().toString());
            usuarioModelo.setSenha(senha.getText().toString());
            int indiceEstado = estado.getSelectedItemPosition();
            usuarioModelo.setEstado(estado.getItemAtPosition(indiceEstado).toString());

            int indiceCidade = cidade.getSelectedItemPosition();
            usuarioModelo.setCidade(cidade.getItemAtPosition(indiceCidade).toString());
            cadastraUsuario();
        }
    }
//
//    private void limparCampos(){
//        nome.setText("");
//        nascimento.setText("");
//        usuario.setText("");
//        email.setText("");
//    }

    private void cadastraUsuario(){
        autenticacao = FirebaseAuth.getInstance();
        autenticacao.createUserWithEmailAndPassword(usuarioModelo.getEmail(), usuarioModelo.getSenha())
                .addOnCompleteListener(UserForm.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
//                    Toast.makeText(UserForm.this, "Usuário cadastrado", Toast.LENGTH_LONG).show();
                                //Recuperando o result da criacao do user, com getUser
                                // pegamos o objeto do tipo FirebaseUser

                    String idUsuario = task.getResult().getUser().getUid();

                    usuarioModelo.setId( idUsuario );

                    usuarioModelo.setNotificationToken(token);

                    usuarioModelo.salvar();



                    preferencia = new Preferencias(UserForm.this);
                    preferencia.salvarUsuarioPreferencias(idUsuario, usuarioModelo.getNome());

                    String imagem_user = upLoadImagem();
                    Log.i("imagem capturada", "imagem capturada: " + imagem_user);
                    usuarioModelo.setImagem(imagem_user);

                    abrirLoginUsuario();
                } else{
                    String erroExcessao = "";
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e){
                        erroExcessao = ", Digite uma senha mais forte, contendo caracteres e números";
                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        erroExcessao = ", E-mail digitado invalido, digite um novo e-mail";
                    } catch (FirebaseAuthUserCollisionException e) {
                        erroExcessao = ", O e-mal ja está em uso no App";
                    } catch (Exception e) {
                        erroExcessao = " ao efetuar cadastro";
                        e.printStackTrace();
                    }
                    Toast.makeText(UserForm.this, "Erro" + erroExcessao, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void abrirLoginUsuario(){
        Intent intent = new Intent(UserForm.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private String upLoadImagem() {
        String imagem = null;
        if (mImageUri != null) {
            mStorage = FirebaseStorage.getInstance().getReference(preferencia.getDadosUsuario().get("identificador"));
            StorageReference fileReference = mStorage.child(usuarioModelo.getId()
                    + "." + getFileExtension(mImageUri));

            imagem = String.valueOf(mStorage.child(usuarioModelo.getId()+ "." +getFileExtension(mImageUri)));

            Log.i("PET_IMAGE", "PET_IMAGE: " + imagem);

            mUpdateTask = fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(UserForm.this,"Updade sucesso",Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }

        return imagem;
    }

    public void getMyToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Token", "getInstanceId failed", task.getException());
                            token = null;
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();

                        // Log
                        Log.d("Token", token);
                    }
                });
    }
}
