package br.com.arthurcvm.adoteme.helper;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class Preferencias {
    private Context contexto;
    private SharedPreferences preferencias;
    private final String NOME_ARQUIVO = "adoteme.prefencias";
    private final int MODE = 0;
    private SharedPreferences.Editor editor;

    private final String CHAVE_IDENTIFICADOR = "identificador";
    private final String NOME_USUARIO = "nome_usuario";

    public Preferencias( Context contextoParametro){
        contexto = contextoParametro;
        preferencias = contexto.getSharedPreferences(NOME_ARQUIVO, MODE);
        editor = preferencias.edit();

    }

    public void salvarUsuarioPreferencias(String identificador, String nome_usuario){
        editor.putString(CHAVE_IDENTIFICADOR, identificador);
        editor.putString(NOME_USUARIO, nome_usuario);
        editor.commit();
    }

    public HashMap<String, String> getDadosUsuario(){
        HashMap<String, String> dadosUsuario = new HashMap<>();
        dadosUsuario.put(CHAVE_IDENTIFICADOR, preferencias.getString(CHAVE_IDENTIFICADOR, null));
        dadosUsuario.put(NOME_USUARIO, preferencias.getString(NOME_USUARIO, null));
        return dadosUsuario;
    }

    public void deleteDadosUsuario(){
        SharedPreferences.Editor preferencias = contexto.getSharedPreferences(NOME_ARQUIVO, MODE).edit();
        preferencias.clear();
        preferencias.commit();
    }

}
